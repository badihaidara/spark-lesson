package com.uie
import com.uie.commons.sparkutils.{get_sparkSession}
import org.apache.spark.sql.DataFrame

object main {

  def main(args: Array[String]): Unit ={

    val spark = get_sparkSession("spark lesson exemples")

    val data = spark.sql("select 1+1;")

    data.show()

    println("affichage data")

    spark.stop()
  }
}
